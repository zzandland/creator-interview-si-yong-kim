import React from 'react';
import { connect } from 'react-redux';

const Navbar = ({ toggleToRepos, toggleToOrgs }) => (
  <div className="navbar">
    <a src="/"><img src="imgs/github-logo.png" alt="home" /></a>
    <div className="toggle-mode">
      <span onClick={toggleToRepos}>Search Repos</span>
      <span onClick={toggleToOrgs}>Search Organizations</span>
    </div>
  </div>
)

const mapDispatchToProps = dispatch => ({
  toggleToRepos: () => {
    dispatch({ type: 'REPOS', mode: 'repos' });
    dispatch({ type: 'RESET' });
  },
  toggleToOrgs: () => {
    dispatch({ type: 'ORGS', mode: 'orgs' });
    dispatch({ type: 'RESET' });
  } 
})

export default connect(
  null,
  mapDispatchToProps,
)(Navbar);
