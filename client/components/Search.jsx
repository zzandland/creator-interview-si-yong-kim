import React, { Component } from 'react';
import { connect } from 'react-redux';

import { asyncGetRepos, asyncGetUserData  } from '../apiAction';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
    }
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleInputSubmit = this.handleInputSubmit.bind(this);
  }

  handleInputChange(e) {
    this.setState({
      query: e.target.value,
    })
  }

  handleInputSubmit(e) {
    const { query } = this.state;
    const { mode, searchRepos, searchOrgs } = this.props;
    e.preventDefault();
    if (mode === 'repos') searchRepos(query);
    else searchOrgs(query);
    this.setState({
      query: '',
    })
  }

  render() {
    const { query } = this.state;
    const { mode, pending } = this.props;
    return (
      <div className="search-main">
        <img src="imgs/github-svg.svg" alt="big-logo" />
        <h1>Let's {(mode === 'repos' ? 'search' : 'get')} some Github {mode}</h1>
        <h3>Enter a username, and we'll dox them right here in front of everyone.</h3>
        <form onSubmit={this.handleInputSubmit}>
          <label>Username</label>
          <input type="text" value={query} onChange={this.handleInputChange}/>
          <div className="search-button">
            {(pending) 
              ? <img src="imgs/pending.gif" alt="pending" />
              : <input type="submit" value="Dox 'em" />
            }
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = ({ mode, pending }) => ({ mode, pending })

const mapDispatchToProps = dispatch => ({
  searchRepos: query => dispatch(asyncGetRepos(query)),
  searchOrgs: query => dispatch(asyncGetUserData(query)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Search);
