import React, { Component } from 'react';
import Navbar from './Navbar';
import Search from './Search';
import Results from './Results';

const App = (props) => (
  <div>
    <Navbar />
    <Search />
    <Results />
  </div>
)

export default App;
