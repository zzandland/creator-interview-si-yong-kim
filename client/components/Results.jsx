import React from 'react';
import { connect } from 'react-redux';

const Results = ({ results }) => (
  <ul className="results">
    {results.length > 0 &&
      results.map(result => (
        <div>
          <h4>{result.name || result.login}</h4>
          <p>{result.description}</p>
        </div>
      ))
    }
  </ul>
)

const mapStateToProps = ({ results }) => ({ results });

export default connect(
  mapStateToProps,
  null,
)(Results);
