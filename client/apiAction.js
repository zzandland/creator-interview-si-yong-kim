import { getRepos, getUserData } from '../api/github-api';

const requestData = bool => ({
  type: 'REQUEST_DATA',
  bool,
})

const handleError = err => ({
  type: 'FAILED_TO_RECEIVE',
  err,
})

export const asyncGetRepos = query => {
  return dispatch => {
    dispatch(requestData(true));
    getRepos(query)
      .then(repos => {
        dispatch(requestData(false));
        dispatch({ type: 'SEARCH_REPOS', repos })
      })
      .catch(err => dispatch(handleError()))
  }
}

export const asyncGetUserData = query => {
  return dispatch => {
    dispatch(requestData(true));
    getUserData(query)
      .then(data => {
        dispatch(requestData(false));
        dispatch({ type: 'SEARCH_ORGS', orgs: data.orgs })
      })
      .catch(err => dispatch(handleError()))
  }
}
