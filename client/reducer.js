import { combineReducers } from 'redux';

const results = (state = [], action) => {
  switch(action.type) {
    case 'SEARCH_REPOS':
      return action.repos;
    
    case 'SEARCH_ORGS':
      return action.orgs;

    case 'RESET':
      return [];

    case 'FAILED_TO_RECEIVE':
      return action.err;

    default: 
      return state;
  }
}

const pending = (state = false, action) => {
  switch(action.type) {
    case 'REQUEST_DATA':
      return action.bool;

    default: 
      return state;
  }
}

const mode = (state = 'repos', action) => {
  switch(action.type) {
    case 'REPOS':
      return action.mode;

    case 'ORGS':
      return action.mode;

    default:
      return state;
  }
}

export default combineReducers({
  results,
  pending,
  mode,
})
